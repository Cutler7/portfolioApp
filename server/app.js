//imports
const   express = require('express'),
        path = require('path'),
        compression = require('compression'),
        bodyParser = require('body-parser'),
        cookieParser = require('cookie-parser'),
        session = require('express-session'),
        csrf = require('csurf'),
        timeout = require('connect-timeout'),
        errorHandler = require('errorhandler'),
        methodOverride = require('method-override'),
        responseTime = require('response-time'),
        favicon = require('serve-favicon'),
        busboy = require('connect-busboy'),
        multer  = require('multer');

const   error = require('./src/error_handling'),
        dbData = require('./src/db_connect');

const app = express();
const upload = multer({ dest: 'uploads/' });
const environment = app.get('env');

//server configuration
app.set('port', 3000);
app.set('view cache', true);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
    if(environment === 'production') {
        app.disable('x-powered-by');
    }

//middleware
app.use(compression({ threshold: 1 }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
    secret: 'pixel view',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}));
//app.use(csrf());
app.use(methodOverride('_method'));
    if(environment == 'production') {
        app.use(responseTime(4));
    }
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(busboy());

//serve static resources
app.use(express.static(path.join(__dirname, 'public')));

//routing
//render page
app.get('*', function(req, res) {
        res.status(200).render('index.ejs', { title: 'Adopt a Pet' });
      });

//data exchange with the database
//display data
app.post('/main/pets', dbData.sendPetsData);
app.post('/main/pets/search', dbData.sendSelectedPetsData);
//settings
app.post('/main/settings/add', upload.single('photo'), dbData.addNewPet);
//contact
app.post('/main/contact', dbData.saveMessage);
//login
app.post('/main/login', dbData.login);

//error handling
if (environment == 'development') {
    app.use(errorHandler());
} else {
    app.use('*', error);
}

app.listen(app.get('port'), () =>   {   
                                        console.log('Listening at port: ' + app.get('port'));
                                        console.log('Environment: ' + environment);
                                    });
