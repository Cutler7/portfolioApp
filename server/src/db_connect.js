const   MongoClient = require('mongodb').MongoClient;

const   imageConvert = require('./binary_data_convert')

let db;
const dbUrl = 'mongodb://localhost:27017/';

MongoClient.connect(dbUrl, (err, client) => {
    if (err) return console.log(err);
    db = client.db('adoptapet');
});


//exchange data main functions
exports.sendPetsData = function(req, res, next) {
    db.collection('pets').find({}).toArray((err, result) => {
        if(err) {
            res.send("Nie można odczytać danych z bazy");
            console.log(error);
        }
        res.json(result);
    });
};

exports.sendSelectedPetsData = function(req, res, next) {
    res.json(req.body);
};

exports.addNewPet = function(req, res, next) {
    let data = req.body;
    if(data.sterilized === undefined) data.sterilized = false;
    if(data.grafted === undefined) data.grafted = false;
    data.dateOfAddition = new Date();
    data.photo = req.file;
    data.photo.bin = {};
    data.photo.bin.bin = imageConvert.binToBase64(data.photo.path);
    console.log(data);
    db.collection('pets').insertOne(data)
    .then(() => res.json(data))
    .catch(() => res.send('zapisywanie nie powiodło się'));
};

exports.saveMessage = function(req, res, next) {
    res.json(req.body);
};

exports.login = function(req, res, next) {
    res.json(req.body);
};
