const express = require('express');
const router = express.Router();

const environment = router.get('env');

router.use(function(req, res, next) {
    var err = new Error('Nie znaleziono.');
    err.status = 404;
    next(err);
});

router.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = router;
