const fs = require('fs');

exports.binToBase64 = function(path) {
    let bin = fs.readFileSync(path)

    return new Buffer(bin).toString('base64');
}
