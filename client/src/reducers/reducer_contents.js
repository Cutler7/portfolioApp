import { contents } from "../../res/consts/contents";

export default function() {
    return {
        mainPage: contents.mainPage,
        about: contents.about,
        contact: contents.contact
    };
}
