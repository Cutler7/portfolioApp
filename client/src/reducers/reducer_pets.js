import { FIND_PET, FETCH_PETS } from "../actions/index";

export default function(state = [], action) {
    switch(action.type) {
        case FIND_PET:
            return action.payload;
        case FETCH_PETS:
            return [...action.payload];  
        default:
            return state;
    }
}
