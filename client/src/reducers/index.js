import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import ContentsReducer from "./reducer_contents";
import PetsReducer from "./reducer_pets";

const rootReducer = combineReducers({
    contents: ContentsReducer,
    pets: PetsReducer,
    form: formReducer
});

export default rootReducer;
