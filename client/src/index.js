import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import promise from "redux-promise";
import { BrowserRouter } from 'react-router-dom';

import App from "./components/app";
import reducers from "./reducers/index";

const Store = applyMiddleware(promise)(createStore);

ReactDom.render(
    <Provider store={Store(reducers)}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>, 
    document.getElementById("react-app")
);
