import React, { Component } from "react";
import { connect } from "react-redux";

class About extends Component {
    
    render() {
        const contents = this.props.contents.about;
        return(
            <div className="content">
                <h1 className="title">
                    {contents.title}
                </h1>
                <div className="info">
                    {contents.info}
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc felis quam,
                     tempus ut tincidunt eget, semper vel nulla. Proin ac risus vel purus efficitur
                      mattis. Donec et augue sed felis hendrerit porttitor. Duis tempor cursus
                       feugiat. Nullam sagittis sem in elit laoreet condimentum. Sed suscipit
                        nec leo vel tincidunt. Aenean erat nibh, porta eu justo et, lacinia varius
                         ipsum. Praesent hendrerit eros neque, eget vulputate magna efficitur eu.
                          Duis ut eros porttitor, sagittis enim sit amet, imperdiet dolor. Morbi
                           vel elit rutrum, mattis felis sed, semper turpis.

                    Vestibulum tortor ipsum, elementum ac eros sit amet,
                     molestie pretium dolor. Aenean varius mi a neque interdum dictum.
                      Praesent ac nulla interdum, pellentesque lorem ac, dictum enim. Vestibulum
                       in ex arcu. Phasellus sed lectus faucibus, vulputate erat ut, tristique
                        neque. Quisque rhoncus tristique eros, ut euismod tortor lacinia nec.
                         Curabitur et ullamcorper leo. Nam tempus aliquet metus. Proin sollicitudin
                          ipsum nec est rhoncus, quis eleifend turpis fermentum. In sodales pretium
                           ornare.

                    Suspendisse potenti. Donec interdum arcu vitae dictum sodales. Curabitur 
                    congue eget ligula non fringilla. Phasellus accumsan id lacus id condimentum.
                     Integer vitae nisl magna. Sed ac eros eu tortor vestibulum ultrices. Integer
                      a congue turpis. In nec viverra mauris, non accumsan dui.</p>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        contents: state.contents
    };
}

export default connect(mapStateToProps)(About);
