import React, { Component } from "react";
import { Link } from "react-router-dom";

class PanelButton extends Component {
    
    render() {
        
        const cname = "panel-button " + this.props.dis;
        
        return(
            <div className={cname}>
                <Link to={this.props.rel}>
                    {this.props.name}
                </Link>
            </div>
        );
    }
}

export default PanelButton;
