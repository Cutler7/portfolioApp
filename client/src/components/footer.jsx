import React, { Component } from "react";

class Footer extends Component {
    render() {
        return(
            <div className="footer">
                Copyright 2018 Daniel Podolak
            </div>
        );
    }
}

export default Footer;
