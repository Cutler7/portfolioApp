import React, { Component } from "react";

import PanelButton from "./panelButton";

class Navbar extends Component {
    
    render() {
        return(
            <div className="main-panel">
                <PanelButton name="PRZEGLĄDAJ" rel="/main/search" dis="left"/>
                <PanelButton name="O NAS" rel="/main/about" dis="left"/>
                <PanelButton name="KONTAKT" rel="/main/contact" dis="left"/>
                <PanelButton name="LOGOWANIE" rel="/main/login" dis="right"/>
                <PanelButton name="S" rel="/main/settings" dis="right"/>
            </div>
        );
    }
}

export default Navbar;
