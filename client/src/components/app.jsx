import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

//layout components
import Logo from "./logo"
import Navbar from "./navbar";
import Footer from "./footer";
import Background from "../../res/images/backgroundImg.jpg";

//router components
import MainPage from "./mainPage";
import Search from "../containers/searchPage";
import About from "./aboutPage";
import Contact from "../containers/contactForm";
import Settings from "../containers/settingsPage";
import Login from "../containers/loginForm";
import AddPetForm from "../containers/addPetForm";

const backgroundImg = {
    backgroundImage: `url(${Background})`,
    backgroundAttachment: "fixed",
    backgroundRepeat: "no-repeat",
    backgroundSize: "100%",
  };

class App extends Component {
    
    render() {
        return(
            <div className="main-container" style={ backgroundImg }>
                <Logo />
                <Navbar />
                <div className="content-container">
                        <Route exact path="/" component={ MainPage } />
                        <Route path="/main/search" component={ Search } />
                        <Route path="/main/about" component={ About } />
                        <Route path="/main/contact" component={ Contact } />
                        <Route exact path="/main/settings" component={ Settings } />
                        <Route path="/main/settings/add" component={ AddPetForm } />
                        <Route path="/main/login" component={ Login } />
                </div>
                <Footer />
            </div>
        );
    }
}

export default App;
