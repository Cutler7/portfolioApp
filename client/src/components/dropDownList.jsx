import React, { Component } from "react";

export default class DropDownList extends Component {
    
    renderSelectOptions = (option) => {
        return (
          <option key={option} value={option}>{option}</option>
        );
      }
    
    render() {
        let val;
        if(this.props.multi) val = [...this.props.input.value]; else val = this.props.input.value;
        return(
                <div>
                    <select {...this.props.input} multiple={this.props.multi} value={val}>
                        {this.props.options.map(this.renderSelectOptions)}
                    </select>
                </div>
        );
    }
}
