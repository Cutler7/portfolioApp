//this file contains ready-made solution from stackoverflow

import React, { Component } from "react";

const adaptFileEventToValue = delegate =>
  e => delegate(e.target.files[0])

export const FileUpload = ({
  input: {
    value: omitValue,
    onChange,
    onBlur,
    ...inputProps,
  },
  meta: omitMeta,
  ...props,
}) =>
  <input
    onChange={adaptFileEventToValue(onChange)}
    onBlur={adaptFileEventToValue(onBlur)}
    type="file"
    {...inputProps}
    {...props}
  />;
