import React, { Component } from "react";
import { connect } from "react-redux";

class MainPage extends Component {
    constructor(props) {
        super(props);
    
        this.state = { clicked: false };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
          clicked: !prevState.clicked
        }));
    }

    render() {
        const contents = this.props.contents.mainPage;
        let projectInfo;

        if(this.state.clicked) {
            projectInfo =   <div className="info">
                                { contents.projectInfo }
                                <br/><br/>
                                <a href="https://gitlab.com/Cutler7/portfolioApp" className="link">
                                    Link do repozytorium
                                </a><br/><br/>
                                Email: { " " }
                                <a href="mailto:podolak9602@gmail.com" className="link">
                                    podolak9602@gmail.com
                                </a>
                            </div>;
        } else {
            projectInfo = null;
        }

        return(
            <div className="content">
                <h1 className="title">
                    {contents.title}
                </h1>
                <div className="info">
                    {contents.info}
                </div>
                { projectInfo }
                <button onClick={ this.handleClick }>
                    { this.state.clicked ? 'Ukryj' : 'Informacje o projekcie...'}
                </button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        contents: state.contents
    };
}

export default connect(mapStateToProps)(MainPage);
