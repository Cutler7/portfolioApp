import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchPets } from "../actions/index";

class Search extends Component {
    componentDidMount() {
        this.props.fetchPets(error => {
            if(error) console.log(error);
        });
    }
    
    renderAnswer(field) {
        if(field == "true") {
            return "tak";
        } else {
            return "nie";
        };
    }

    //src='data:image/jpeg;base64, LzlqLzRBQ...<!-- base64 data -->' />
    renderFields() {
        return this.props.pets.map(field => 
            <tr key={field._id}>
                <td>
                    <div>
                        <img 
                            src={"data:" + field.photo.mimetype + ";base64, " + field.photo.bin.bin} 
                            alt="photo"
                            className="pet-photo"
                        />
                    </div>
                    <div>
                        {field.name}
                        {field.dateOfAddition}
                        <ul>
                            <li>Płeć:{' '}{field.sex}</li>
                            <li>Wiek:{' '}{field.age}</li>
                            <li>Szczepiony(a):{' '}{this.renderAnswer(field.grafted)}</li>
                            <li>Wysterylizowany(a):{' '}{this.renderAnswer(field.sterilized)}</li>
                            <li>Charakter:{' '}</li>
                            <li>Opis:{' '}{field.description}</li>
                        </ul>
                    </div>
                </td>
            </tr>
        );
    }

    render() {
        console.log(this.props.pets)
        return(
            <div className="content-fullWidth">
            <table>
                <tbody>
                    {this.renderFields()}
                </tbody>
            </table>
            </div>
        );
    }
}

const mapStateToProps = state => ({pets: state.pets});

const mapDispatchToProps = dispatch => bindActionCreators(
    { fetchPets },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Search);
