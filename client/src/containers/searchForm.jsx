import React, { Component } from "react";
import { connect } from "react-redux"
import { Field, reduxForm } from "redux-form";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";

import { findPet } from "../actions/index";

class SearchForm extends Component {

    onSubmit(values) {
        this.props.findPet(values, (error) => {
            if(error) {
                console.log(error);
            } else {
                this.props.history.push("/");
            }
        });
      }
    
    render() {
        const { handleSubmit } = this.props;

        return(
            <div className="content searchForm">
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field
                        name="pet_name"
                        label="imię zwierzaka"
                        style="input text"
                        type="text"
                        component={this.renderField}
                    />
                    <Field
                        name="min_age"
                        label="wiek (od)"
                        style="input number"
                        type="text"
                        component={this.renderField}
                    />
                    <Field
                        name="max_age"
                        label="wiek (do)"
                        style="input number"
                        type="text"
                        component={this.renderField}
                    />
                    
                    <div>
                        <button type="submit">Szukaj</button>
                    </div>
                </form>
            </div>
        );
    }
        

}

const mapDispatchToProps = dispatch => bindActionCreators(
    { findPet },
    dispatch
);

export default reduxForm({
    form: "SearchPetForm"
})(connect(null, mapDispatchToProps)(SearchForm));
