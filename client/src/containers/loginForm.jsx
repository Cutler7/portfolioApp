import React, { Component } from "react";
import { connect } from "react-redux"
import { Field, reduxForm } from "redux-form";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";

import { login } from "../actions/index";

class LoginForm extends Component {

    onSubmit(values) {
        this.props.login(values, () => {
          this.props.history.push("/");
        });
      }
    
    render() {
        const { handleSubmit } = this.props;

        return(
            <div className="content loginForm">
                <form onSubmit = { handleSubmit(this.onSubmit.bind(this)) }>
                    <div>
                        <label>Login</label>
                        <div>
                            <Field
                                name="login"
                                component="input"
                                type="text"
                                placeholder="login"
                            />
                        </div>
                    </div>
                    <div>
                        <label>Hasło</label>
                        <div>
                            <Field
                                name="password"
                                component="input"
                                type="password"
                                placeholder="password"
                            />
                        </div>
                    </div>
                    <div>
                        <label></label>
                        <button><Link to="/">Anuluj</Link></button>
                        <button type="submit">Zaloguj</button>
                    </div>
                </form>
            </div>
        );
    }
        

}

const mapDispatchToProps = dispatch => bindActionCreators(
    { login },
    dispatch
);

export default reduxForm({
    form: "LoginForm"
})(connect(null, mapDispatchToProps)(LoginForm));
