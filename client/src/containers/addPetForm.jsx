import React, { Component } from "react";
import { connect } from "react-redux"
import { Field, reduxForm, formValueSelector } from "redux-form";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";

import { addPet } from "../actions/index";
import { dogCharacter, catCharacter } from "../../res/consts/character";
import DropDownList from "../components/dropDownList";
import { FileUpload } from "../components/fileUpload";

class AddPetForm extends Component {

    onSubmit(values) {
        console.log(values);
        this.props.addPet(values, (response, error) => {
            if(error) {
                console.log(error);
            } else {
                console.log(response);
                this.props.history.push('/main/settings');
            }
        });
    }

    render() {
        const { handleSubmit, reset, typeValue } = this.props;

        return(
            <div className="content form">
                <form 
                    onSubmit = { handleSubmit(this.onSubmit.bind(this)) }
                    encType="multipart/form-data"
                >
                    {/*zwierzę: pies/kot */}
                    <div>
                        <label></label>
                        <div>
                        <label className="type-check">
                            <Field
                            name="type"
                            component="input"
                            type="radio"
                            value="pies"
                            />{' '}
                            Pies
                        </label>
                        <label className="type-check">
                            <Field
                            name="type"
                            component="input"
                            type="radio"
                            value="kot"
                            />{' '}
                            Kot
                        </label>
                        </div>
                    </div>
                    {/* imię */}
                    <div>
                        <label>Imię</label>
                        <div>
                            <Field
                                name="name"
                                type="text"
                                component="input"
                            />
                        </div>
                    </div>
                    {/* pies: płeć */}
                    {(typeValue)=="pies" && (
                    <div>
                        <label>Płeć</label>     
                        <div>
                        <label className="radio">
                            <Field
                            name="sex"
                            component="input"
                            type="radio"
                            value="pies"
                            />{' '}
                            Pies
                        </label>
                        <label className="radio">
                            <Field
                            name="sex"
                            component="input"
                            type="radio"
                            value="suka"
                            />{' '}
                            Suka
                        </label>
                        </div>
                    </div>
                    )}
                    {/* kot: płeć */}
                    {(typeValue)=="kot" && (
                    <div>
                        <label>Płeć</label>    
                        <div>
                        <label className="radio">
                            <Field
                            name="sex"
                            component="input"
                            type="radio"
                            value="kot"
                            />{' '}
                            Kot
                        </label>
                        <label className="radio">
                            <Field
                            name="sex"
                            component="input"
                            type="radio"
                            value="kotka"
                            />{' '}
                            Kotka
                        </label>
                        </div>
                    </div>
                    )}
                    {/* wiek */}
                    <div>
                        <label>Wiek</label>
                        <div>
                        <label className="radio">
                            <Field
                            name="age"
                            component="input"
                            type="radio"
                            value="szczeniak / kocię"
                            />{' '}
                            Szczeniak{' / '}Kocię
                        </label>
                        <label className="radio">
                            <Field
                            name="age"
                            component="input"
                            type="radio"
                            value="dorosły"
                            />{' '}
                            Dorosły
                        </label>
                        <label className="radio">
                            <Field
                            name="age"
                            component="input"
                            type="radio"
                            value="senior"
                            />{' '}
                            Senior
                        </label>
                        </div>
                    </div>
                    {/* pies: charakter */}
                    {(typeValue)=="pies" && (
                    <div>
                        <label>Charakter</label>
                        <div>
                            <Field
                                name="character"
                                component={DropDownList}
                                multi={true}
                                options={dogCharacter}
                            />
                        </div>
                    </div>
                    )}
                    {/* kot: charakter */}
                    {(typeValue)=="kot" && (
                    <div>
                        <label>Charakter</label>
                        <div>
                            <Field
                                name="character"
                                component={DropDownList}
                                multi={true}
                                options={catCharacter}
                            />
                        </div>
                    </div>
                    )}
                    {/* szczepiony? */}
                    <div>
                        <label>Szczepiony</label>
                        <div>
                        <Field
                            name="grafted"
                            id="grafted"
                            component="input"
                            type="checkbox"
                        />
                        </div>
                    </div>
                    {/* wysterylizowany? */}
                    <div>
                        <label>Wysterylizowany</label>
                        <div>
                        <Field
                            name="sterilized"
                            id="sterilized"
                            component="input"
                            type="checkbox"
                        />
                        </div>
                    </div>
                    {/* zdjęcie */}
                    <div>
                        <label>Zdjęcie</label>
                        <div>
                            <Field
                                name="photo"
                                accept="image/gif, image/jpeg, image/png"
                                component={FileUpload}
                            />
                        </div>
                    </div>
                    {/* opis */}
                    <div>
                        <label>Opis</label>
                        <div>
                            <Field
                                name="description"
                                component="textarea"
                            />
                        </div>
                    </div>
                    <div>
                        <button><Link to="/main/settings">Anuluj</Link></button>
                        <button type="button" onClick={reset}>Wyczyść formularz</button>
                        <button type="submit">Dodaj</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(
    { addPet },
    dispatch
);

AddPetForm = reduxForm({
    form: "AddPetForm"
})(AddPetForm);

const selector = formValueSelector('AddPetForm');

AddPetForm = connect(state => {
    const typeValue = selector(state, 'type');
    return { typeValue };
}, mapDispatchToProps)(AddPetForm);

export default AddPetForm;
