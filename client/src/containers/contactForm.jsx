import React, { Component } from "react";
import { connect } from "react-redux"
import { Field, reduxForm } from "redux-form";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";

import { sendMessage } from "../actions/index";

class Contact extends Component {

    onSubmit(values) {
        this.props.sendMessage(values, () => {
          this.props.history.push("/");
        });
    }

    render() {
        const { handleSubmit } = this.props;

        return(
            <div className="content contactForm">
                <form onSubmit = { handleSubmit(this.onSubmit.bind(this)) }>
                    <div>
                        <label>Wiadomość</label>
                        <div>
                            <Field
                                name="message"
                                component="textarea"
                            />
                        </div>
                    </div>
                    <div>
                        <label>Imię</label>
                        <div>
                            <Field
                                name="name"
                                component="input"
                                type="text"
                                placeholder="Imię"
                            />
                        </div>
                    </div>
                    <div>
                        <label>Email</label>
                        <div>
                            <Field
                                name="email"
                                component="input"
                                type="email"
                                placeholder="Email"
                            />
                        </div>
                    </div>
                    <div>
                        <label></label>
                        <button><Link to="/">Anuluj</Link></button>
                        <button type="submit">Wyślij</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(
    { sendMessage },
    dispatch
);

export default reduxForm({
    form: "ContactForm"
})(connect(null, mapDispatchToProps)(Contact));
