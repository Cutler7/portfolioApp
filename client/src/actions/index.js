import axios from "axios";

export const FIND_PET = "find_pet";
export const FETCH_PETS = "fetch_pets";
export const ADD_PET = "add_pet";
export const USER_LOGIN = "user_login";
export const SEND_MESSAGE = "send_message";

export function findPet(values, callback) {
    const request = axios.post('/main/search', values)
        .then(() => callback(null))
        .catch(error => callback(error));

    return {
        type: FIND_PET,
        payload: request
    };
}

export function fetchPets(callback) {
    const request = axios.post('/main/pets')
        .then(function(response) { 
            console.log(response.data)
            return response.data })
        .catch(error => callback(error));
    return {
        type: FETCH_PETS,
        payload: request
    };
}

export function addPet(values, callback) {
    const formData = new FormData();
    for(let i in values) formData.append(i, values[i]);
    console.log(formData, values.photo);
    const request = axios
        .post('/main/settings/add', formData)
        .then((response) => callback(response))
        .catch((error) => callback(null, error));
    return {
        type: ADD_PET,
        payload: request
    };
}

export function sendMessage(values, callback) {
    const request = axios
        .post('/main/contact', values)
        .then(() => callback())
        .catch(function (error) {
            console.log(error);
        });
    console.log(request);
    return {
        type: SEND_MESSAGE,
        payload: request
    };
}

export function login(values, callback) {
    const request = axios
        .post('/main/login', values)
        .then(() => callback())
        .catch(function (error) {
            console.log(error);
        });
    
    return {
        type: USER_LOGIN,
        payload: request
    };
}
