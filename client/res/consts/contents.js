export const contents = {
    about: {
        title: "O Nas",
        info: "informacje o stronie",
    },
    contact: {
        title: "Kontakt",
        info: "link do repo + email"
    },
    mainPage: {
        title: "Strona Główna",
        info: "Portal schroniska dla zwierząt ",
        projectInfo: "Ten projekt powstał w celu prezentacji moich umiejętności.",
    }
}
