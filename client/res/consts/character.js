export const dogCharacter = [
    "łagodny",
    "akceptuje dzieci",
    "akceptuje inne psy",
    "nie załatwia się w domu",
    "nie niszczy przedmiotów",
    "pieszczoch"
];

export const catCharacter = [
    "towarzyski",
    "leniuch",
    "przytulas/pieszczoch",
    "nie niszczy przedmiotów",
    "łowczy instynkt",
    "akceptuje inne zwierzęta"
];