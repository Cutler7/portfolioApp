# My Portfolio Web Application

## To run this locally:

1. Clone repository using this command:
    ```sh
    $ git clone https://gitlab.com/Cutler7/portfolioApp.git
    ```
or
download the .zip file and unpack this in project folder.
    
2. If you don't have Node.js installed use this command:
    ```sh
    curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
    sudo apt-get install -y nodejs
    ```
in Windows download installation file from [https://nodejs.org/en/download](https://nodejs.org/en/download/)
and install it.

3. In project folder install required packages:
    ```sh
    $ npm install
    ```
    
4. Compile project using webpack:
    ```sh
    $ npm run build
    ```
    
5. Run app:
    ```sh
    $ npm start
    ```
    
The app running at http://localhost:3000

## To run only the client-side:

* Do steps 1-3 and run following command:
    
    ```sh
    $ npm test
    ```
    
The app running at http://localhost:8080
