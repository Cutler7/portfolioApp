const path = require('path');


module.exports = {
  entry: [
    path.join(__dirname, '/client/src/index.js'),
    path.join(__dirname, '/client/res/styles/main.scss')
  ],

  output: {
    path: path.join(__dirname, '/server/public'),
    filename: 'js/bundle.js',
    publicPath: "/"
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
            presets: ["react", "es2015", "stage-1"]
          }
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            outputPath: "images/",
          }
        }]
      }
    ]
  },

  resolve: {
    extensions: [".js", ".jsx", "scss"]
  },

  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, "/server/public")
  }
};
